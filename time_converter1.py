import re


def time_converter(time):
    stime = re.split(' |:', time)
    stime[0] = '00' if stime[0] == '12' and stime[2] == 'a.m.' else stime[0]
    return '{:02}'.format(int(stime[0] if stime[2] == 'a.m.' or stime[0] == '12'
                              else int(stime[0])+12))+":"+stime[1]


if __name__ == "__main__":
    print("Example:")
    print(time_converter("12:30 p.m."))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert time_converter("12:30 p.m.") == "12:30"
    assert time_converter("9:00 a.m.") == "09:00"
    assert time_converter("11:15 p.m.") == "23:15"
    assert time_converter("12:00 a.m.") == "00:00"
    print("Coding complete? Click 'Check' to earn cool rewards!")
