def checkio(text, word):
    mass = []
    for line in text.splitlines():
        mass.append(line.translate({ord(i): None for i in ' '}).lower())
    for ind in range(len(mass)):
        index = mass[ind].find(word)
        if index != -1:
            return [ind + 1, index + 1, ind + 1, index + len(word)]
    for ind in range(max(len(i) for i in mass)):
        index = "".join(i[ind] if ind < len(i) else '' for i in mass).find(word)
        if index != -1:
            return [index + 1, ind + 1, index + len(word), ind + 1]
    return [1, 1, 1, 4]


# These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("""He took his vorpal sword in hand:
Long time the manxome foe he sought--
So rested he by the Tumtum tree,
And stood awhile in thought.
And as in uffish thought he stood,
The Jabberwock, with eyes of flame,
Came whiffling through the tulgey wood,
And burbled as it came!""", "noir") == [4, 16, 7, 16]
    assert checkio("""DREAMING of apples on a wall,
And dreaming often, dear,
I dreamed that, if I counted all,
-How many would appear?""", "ten") == [2, 14, 2, 16]
    assert checkio("xa\nxb\nx", "ab") == [1, 2, 2, 2]
print("Coding complete? Click 'Check' to earn cool rewards!")
