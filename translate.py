VOWELS = "aeiouy"


def translate(phrase):
    result = ""
    index = 0
    while index < len(phrase):
        result = "".join((result, phrase[index]))
        if phrase[index] == " ":
            index += 1
        elif phrase[index] in VOWELS:
            index += 3
        else:
            index += 2
    return result


if __name__ == '__main__':
    print("Example:")
    print(translate("hieeelalaooo"))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert translate("hieeelalaooo") == "hello", "Hi!"
    assert translate("hoooowe yyyooouuu duoooiiine") == "how you doin", "Joey?"
    assert translate("aaa bo cy da eee fe") == "a b c d e f", "Alphabet"
    assert translate("sooooso aaaaaaaaa") == "sos aaa", "Mayday, mayday"
    print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")
