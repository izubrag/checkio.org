def int_palindrome(number: int, B: int) -> bool:
    res = lambda n: [str(n%B)] if n//B == 0 else res(n//B) + [str(n%B)]
    return res(number) == res(number)[::-1]


print("Example:")
print(int_palindrome(455, 2))

# These "asserts" are used for self-checking
assert int_palindrome(6, 2) == False
assert int_palindrome(34, 2) == False
assert int_palindrome(455, 2) == True
assert int_palindrome(157, 100) == False
assert int_palindrome(3148, 16) == True

print("The mission is done! Click 'Check Solution' to earn rewards!")
