from typing import Iterable


def frequency_sorting(numbers: list[int]) -> Iterable[int]:
    frq = {i:numbers.count(i) for i in sorted(numbers)}
    res=[]
    for i in sorted(frq.values(), reverse=True):
        res += [list(frq.keys())[list(frq.values()).index(i)]]*i
        del frq[list(frq.keys())[list(frq.values()).index(i)]]
    return res


print("Example:")
print(list(frequency_sorting([1, 2, 3, 3, 4, 5])))

assert list(frequency_sorting([1, 2, 3, 4, 5])) == [1, 2, 3, 4, 5]
assert list(frequency_sorting([3, 4, 11, 13, 11, 4, 4, 7, 3])) == [
    4,
    4,
    4,
    3,
    3,
    11,
    11,
    7,
    13,
]

print("The mission is done! Click 'Check Solution' to earn rewards!")
