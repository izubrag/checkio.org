def time_converter(time):
    ba = ["a.m.", "p.m."]
    bin_time = tuple(int(i) for i in time.split(':'))
    return "{0:g}:{1:02d} {2:s}".format(bin_time[0]%12 if bin_time[0]%12 != 0 else 12, bin_time[1], ba[bin_time[0]//12])


if __name__ == '__main__':
    print("Example:")
    print(time_converter('12:30'))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert time_converter('12:30') == '12:30 p.m.'
    assert time_converter('09:00') == '9:00 a.m.'
    assert time_converter('23:15') == '11:15 p.m.'
    print("Coding complete? Click 'Check' to earn cool rewards!")
