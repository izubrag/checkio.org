def changing_direction(elements: list[int]) -> int:
    temp = [0 if elements[n] < elements[n+1] else 1 for n in range(len(elements)-1) if elements[n] != elements[n+1]]
    return len([n for n in range(len(temp)-1) if temp[n] != temp[n+1]])


print("Example:")
print(changing_direction([1, 2, 3, 4, 5]))
print(changing_direction([1, 2, 2, 1, 2, 2]))

# These "asserts" are used for self-checking
assert changing_direction([1, 2, 3, 4, 5]) == 0
assert changing_direction([1, 2, 3, 2, 1]) == 1
assert changing_direction([1, 2, 2, 1, 2, 2]) == 2

print("The mission is done! Click 'Check Solution' to earn rewards!")
