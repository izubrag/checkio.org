def frequency_sort(items):
    uniq_elems = {m: items.count(m) for m in items}
    frequency = sorted(uniq_elems.values(), reverse= True)
    items_sorted = []
    for value in frequency:
        key = list(uniq_elems.keys())[list(uniq_elems.values()).index(value)]
        items_sorted.extend([key for x in range(value)])
        uniq_elems[key] = 0
    return items_sorted


if __name__ == '__main__':
    print("Example:")
    print(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4]))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert list(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4])) == [4, 4, 4, 4, 6, 6, 2, 2]
    assert list(frequency_sort(['bob', 'bob', 'carl', 'alex', 'bob'])) == ['bob', 'bob', 'bob', 'carl', 'alex']
    assert list(frequency_sort([17, 99, 42])) == [17, 99, 42]
    assert list(frequency_sort([])) == []
    assert list(frequency_sort([1])) == [1]
    assert list(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4, 2])) == [4, 4, 4, 4, 2, 2, 2, 6, 6]
    print("Coding complete? Click 'Check' to earn cool rewards!")
