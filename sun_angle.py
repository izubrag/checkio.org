def sun_angle(time):
    minuts = lambda x: int(x.split(':')[0])*60+int(x.split(':')[1])
    sunrise = minuts("06:00")
    sunset = minuts("18:00")
    return 180/(sunset-sunrise)*(minuts(time)-sunrise) if sunrise <= minuts(time) <= sunset else "I don't see the sun!"


if __name__ == '__main__':
    print("Example:")
    print(sun_angle("07:00"))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert sun_angle("07:00") == 15
    assert sun_angle("01:23") == "I don't see the sun!"
    print("Coding complete? Click 'Check' to earn cool rewards!")
