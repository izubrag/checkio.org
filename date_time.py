def date_time(time: str) -> str:
    months = ["", "January", "February", "March", "April", "May", "June", "July", "August",
              "September", "October", "November", "December"]
    aaa = lambda x, y: time.split()[x].split(y)
    date1 = "{} {} {} year".format(int(aaa(0, '.')[0]), months[int(aaa(0, '.')[1])], aaa(0, '.')[2])
    time1 = " ".join((str(int(aaa(1, ':')[0])),
                    "hours" if int(aaa(1, ':')[0]) != 1 else "hour",
                    str(int(aaa(1, ':')[1])),
                    "minutes" if int(aaa(1, ':')[1]) != 1 else "minute"))
    return " ".join((date1, time1))


if __name__ == '__main__':
    print("Example:")
    print(date_time('01.01.2000 00:00'))

    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert date_time("01.01.2000 00:00") == "1 January 2000 year 0 hours 0 minutes", "Millenium"
    assert date_time("09.05.1945 06:30") == "9 May 1945 year 6 hours 30 minutes", "Victory"
    assert date_time("20.11.1990 03:55") == "20 November 1990 year 3 hours 55 minutes", "Somebody was born"
    assert date_time("11.04.1812 01:01") == "11 April 1812 year 1 hour 1 minute"
    print("Coding complete? Click 'Check' to earn cool rewards!")
