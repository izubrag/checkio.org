from typing import List


# x,y <- 3-y,x
def rotate(grille: List[str]) -> List[str]:
    newgrille = [[grille[3-y][x] for y in range(len(grille[0]))] for x in range(len(grille))]
    return list(map("".join, newgrille))


def recall_password(grille: List[str], password: List[str]) -> str:
    result = ""
    for i in range(0, len(password)):
        result = result + "".join(list(map(lambda x, y: "".join((y[m] for m in range(0, len(y)) if x[m] == 'X')), grille, password)))
        grille = rotate(grille)
    return result


if __name__ == '__main__':
    print("Example:")
    print(recall_password(['X...', '..X.', 'X..X', '....'],
 ['itdf', 'gdce', 'aton', 'qrdi']))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert recall_password(['X...', '..X.', 'X..X', '....'],
 ['itdf', 'gdce', 'aton', 'qrdi']) == 'icantforgetiddqd'
    assert recall_password(['....', 'X..X', '.X..', '...X'],
 ['xhwc', 'rsqx', 'xqzz', 'fyzr']) == 'rxqrwsfzxqxzhczy'
    print("Coding complete? Click 'Check' to earn cool rewards!")
