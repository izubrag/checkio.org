def missing_number(items: list[int]) -> int:
    items.sort()
    step = min(list([items[n+1] - items[n] for n in range(len(items)-1)]))
    items_full = list(range(min(items), max(items)+step, step))
    return (set(items_full) - set(items)).pop()


print("Example:")
print(missing_number([1, 4, 2, 5]))
print(missing_number([2, 6, 8]))

# These "asserts" are used for self-checking
assert missing_number([1, 4, 2, 5]) == 3
assert missing_number([2, 6, 8]) == 4

print("The mission is done! Click 'Check Solution' to earn rewards!")
