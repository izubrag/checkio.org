import unicodedata


def checkio(in_string):
    """remove accents"""
    temp = []
    for letter in in_string:
        print("1111", unicodedata.name(letter))
        print("2222",  " ".join(unicodedata.name(letter).split(" ")[:4]))
        print("3333", unicodedata.lookup(" ".join(unicodedata.name(letter).split(" ")[:4])))
        norm = unicodedata.name(letter)
        print("4444", "DADADA" if "LETTER" in norm else "NONONO")
        if "COMBINING" not in norm:
            temp.append(unicodedata.lookup(" ".join(norm.split(" ")[:4])))

    print(temp)
    print(''.join(temp))
    ttt = ''.join((unicodedata.lookup(" ".join(unicodedata.name(m).split(" ")[:4])) for m in in_string))
    print(ttt)
    return ''.join(temp)

    # These "asserts" using only for self-checking and not necessary for auto-testing


if __name__ == '__main__':
    assert checkio(u"préfèrent") == u"preferent"
    assert checkio(u"loài trăn lớn") == u"loai tran lon"
    assert checkio(u"完好無缺") == "完好無缺"
    assert checkio("!@#$%^&*()_+,./<>?") == "!@#$%^&*()_+,./<>?"
    print('Done')
