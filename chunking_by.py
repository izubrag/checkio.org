from typing import Iterable


def chunking_by(items: list, size: int) -> Iterable:
    print([items[i:i+size] for i in range(0, len(items), size)])
    return [items[i:i+size] for i in range(0, len(items), size)]


print("Example:")
print(list(chunking_by([5, 4, 7, 3, 4, 5, 4], 3)))

assert list(chunking_by([5, 4, 7, 3, 4, 5, 4], 3)) == [[5, 4, 7], [3, 4, 5], [4]]
assert list(chunking_by([3, 4, 5], 1)) == [[3], [4], [5]]
assert list(chunking_by([5, 4], 7)) == [[5, 4]]
assert list(chunking_by([], 3)) == []
assert list(chunking_by([4, 4, 4, 4], 4)) == [[4, 4, 4, 4]]

print("The mission is done! Click 'Check Solution' to earn rewards!")
