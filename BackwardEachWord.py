def backward_string_by_word(text: str) -> str:
    words = list()
    for word in text.split():
        if len(word) > 1:
            words.append(word[::-1])
            text = text.replace(word, "{}")
    return text.format(*words)


if __name__ == '__main__':
    print("Example:")
    print(backward_string_by_word(''))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert backward_string_by_word('') == ''
    assert backward_string_by_word('world') == 'dlrow'
    assert backward_string_by_word('hello world') == 'olleh dlrow'
    assert backward_string_by_word('hello   world') == 'olleh   dlrow'
    assert backward_string_by_word('welcome to a game') == 'emoclew ot a emag'
    assert backward_string_by_word('olleH Hello') == 'Hello olleH'
    print("Coding complete? Click 'Check' to earn cool rewards!")
