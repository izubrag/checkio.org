import re

brackets = "(){}[]"
dbrackets = {"{": "}", "}": "{", "(": ")", ")": "(", "[": "]", "]": "["}
regexp = "\([^\(\)]*\)|\{[^\{\}]*\}|\[[^\[\]]*\]"


def revert(line: str) -> str:
    print("REVERT BEFORE = ", line)
    print("REVERT AFTER = ", "".join((dbrackets[m] for m in line[::-1])))
    return "".join((dbrackets[m] for m in line[::-1]))


# Remove all brackets
def rab(line: str) -> str:
    return line[0] + "".join((m for m in line[1:-1] if m not in brackets)) + line[-1]


def one_step(line: str, index: int) -> str:
    print("LINE1 = " + line)
    print("FINDALL = ", re.search(regexp, line))
    lookup = re.search(regexp, line)
    if lookup is None:
        return "".join((m for m in line if m not in brackets))
    while True:
        temp = re.search(regexp, lookup.group()[1:-1])
        if temp is None:
            break
        lookup = temp
    line = line.replace(lookup.group(), str(index))
    line = one_step(line, index + 1)
    print("LINE2 = ", line.replace(str(index), "".join((lookup.group()[0], dbrackets[lookup.group()[0]]))))
    return line.replace(str(index), rab(lookup.group()))


def remove_brackets(line: str) -> str:
    print("START = ", line)
    return revert(one_step(revert(line), 0))


if __name__ == '__main__':
    print("Example:")
    print(remove_brackets('(()()'))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert remove_brackets('(()()') == '()()'
    assert remove_brackets('[][[[') == '[]'
    assert remove_brackets('[[(}]]') == '[[]]'
    assert remove_brackets('[[{}()]]') == '[[{}()]]'
    assert remove_brackets('[[[[[[') == ''
    assert remove_brackets('[[[[}') == ''
    assert remove_brackets('') == ''
    assert remove_brackets('[(])') == '()'
    print("Coding complete? Click 'Check' to earn cool rewards!")
