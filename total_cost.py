from typing import List


def total_cost(calls: List[str]) -> int:
    dct = dict()
    for call in calls:
        lst = call.split()
        lst[2] = int(lst[2])//60 + (0 if int(lst[2])%60 == 0 else 1)
        dct[lst[0]] = lst[2] if lst[0] not in dct else dct[lst[0]]+lst[2]
    return sum(map(lambda x: x if x <= 100 else 100 + (x-100)*2, dct.values()))


if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert total_cost(("2014-01-01 01:12:13 181",
                       "2014-01-02 20:11:10 600",
                       "2014-01-03 01:12:13 6009",
                       "2014-01-03 12:13:55 200")) == 124, "Base example"
    assert total_cost(("2014-02-05 01:00:00 1",
                       "2014-02-05 02:00:00 1",
                       "2014-02-05 03:00:00 1",
                       "2014-02-05 04:00:00 1")) == 4, "Short calls but money..."
    assert total_cost(("2014-02-05 01:00:00 60",
                       "2014-02-05 02:00:00 60",
                       "2014-02-05 03:00:00 60",
                       "2014-02-05 04:00:00 6000")) == 106, "Precise calls"
