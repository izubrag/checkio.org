def unix_match(filename: str, pattern: str) -> bool:
    for ind in range(len(pattern)):
        match pattern[ind]:
            case '*':
                if len(pattern[ind:].split('*')[1].split('?')[0]) == 0:
                    filename = str()
                filename = filename[filename.find(pattern[ind:].split('*')[1].split('?')[0]):]
                print(len(filename), filename, len(pattern[ind:].split('*')[1].split('?')[0]))
            case '?':
                if len(filename) == 0:
                    return False
                filename = filename[1:]
            case other:
                if len(filename) == 0 or pattern[ind] != filename[0]:
                    return False
                else:
                    filename = filename[1:]
    return False if len(filename) != 0 else True


print("Example:")
print(unix_match("somefile.txt", "*"))
print(unix_match("log1.txt", "log?.txt"))

# These "asserts" are used for self-checking
assert unix_match("somefile.txt", "*") == True
assert unix_match("other.exe", "*") == True
assert unix_match("my.exe", "*.txt") == False
assert unix_match("log1.txt", "log?.txt") == True
assert unix_match("log12.txt", "log?.txt") == False
assert unix_match("log12.txt", "log??.txt") == True
assert unix_match('txt', '????*') == False

print("The mission is done! Click 'Check Solution' to earn rewards!")
